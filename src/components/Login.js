import React, { useState, useContext } from "react";
import axios from "axios";
import { AuthContext } from "../contexts/authContext";
import Navbar from "./Navbar";

function Login(props) {
  const authContext = useContext(AuthContext);

  const [state, setState] = useState({ username: "" }); //useState is a Hook used to update the state

  function handleChange(event) {
    //runs on every keystroke to update the React state, the displayed value will update as the user types.
    setState({
      ...state,
      [event.currentTarget.name]: event.currentTarget.value,
    });
  }

  async function handleSubmit(event) {
    event.preventDefault();

    try {
      const responseUsername = await axios.get(
        //searchs if the user is stored in a node.js database
        "http://localhost:8000/users?username=" + state.username
      );
      let userbyName = responseUsername.data[0];

      if (!userbyName) {
        const resultCreate = await axios.post(`http://localhost:8000/users`, {
          //if the user does not exist it is created
          username: state.username,
          translations: [],
        });

        userbyName = resultCreate.data;
        setState(userbyName); //keep the user name
      }

      authContext.setLoggedInUser({ user: userbyName });
      localStorage.setItem(
        "loggedInUser",
        JSON.stringify({ user: userbyName })
      );

      const { id } = userbyName;
      props.history.push(`/translate/${id}`); //once logged in, the user is redirected to Translate Page
    } catch (err) {
      console.error(err);
    }
  }

  return (
    <div className="loginSection">
      <Navbar />
      <div className="container loginContainer">
        <form onSubmit={handleSubmit} className="col">
          {/* Username */}
          <h3 className="row getStarted">Let's get started!</h3>
          <div className="input-group mb-3 row">
            <input
              type="text"
              className="form-control col-6"
              name="username"
              id="loginForm"
              placeholder="What's your name?"
              value={state.username}
              onChange={handleChange}
              aria-label="username"
              aria-describedby="button-addon2"
            />
            <button
              className="btn btn-primary col-2"
              type="submit"
              id="button-addon2"
            >
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
