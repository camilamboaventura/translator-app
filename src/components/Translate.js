import React, { useContext, useState } from "react";
import axios from "axios";
import NavbarLogged from "./NavbarLogged";

import { AuthContext } from "../contexts/authContext";

function Translate(props) {
  const authContext = useContext(AuthContext); //useContext” hook is used to create common data that can be accessed throughout the component hierarchy without passing the props down manually to each level
  const [state, setState] = useState({ textInput: "", letters: [] });

  function handleChange(event) {
    setState({
      ...state,
      [event.currentTarget.name]: event.currentTarget.value,
    });
  }

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      let userGet = await axios.get(
        //gets the words entered into the input by the user
        `http://localhost:8000/users/${props.match.params.userId}`
      );
      let user = userGet.data;
      user.translations.push(state.textInput);

      // Updating the loggeding user with the new translation
      authContext.setLoggedInUser({ user: user });

      await axios.put(
        `http://localhost:8000/users/${props.match.params.userId}`,
        user
      );
    } catch (err) {
      console.error(err);
    }

    let lettersArr = state.textInput.toLowerCase().split(""); //the string is split between each character
    setState({
      //updates the state
      ...state,
      letters: lettersArr,
    });
  }

  return (
    <div className="translationSection">
      <NavbarLogged />
      {/* Input used by the user to add a text to translate */}
      <div className="translatePage">
        <form
          className="input-group mb-3 container translate"
          onSubmit={handleSubmit}
        >
          <input
            type="text"
            id="text"
            className="form-control"
            placeholder="What do you want to translate?"
            aria-label="Text to translate"
            aria-describedby="button-addon2"
            maxLength={40}
            name="textInput"
            value={state.textInput}
            onChange={handleChange}
          />
          <button className="btn btn-primary" type="submit" id="button-addon2">
            Translate
          </button>
        </form>
        <div className="signsBox">
          {state.letters.map((letter, index) => {
            //the map runs through all the letters and returns the respective signs
            if (letter === " ") {
              return <br />; //inserts a single line break between the words
            }
            return (
              <img
                src={process.env.PUBLIC_URL + `/individial_signs/${letter}.png`}
                alt="cast"
                key={index}
                className="imgSings"
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default Translate;
