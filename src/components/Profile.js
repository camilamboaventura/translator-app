import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../contexts/authContext";
import axios from "axios";
import NavbarLogged from "./NavbarLogged";

function Profile(props) {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);

  async function handleDelete(event) {
    event.preventDefault();

    let user = { ...loggedInUser.user, translations: [] }; //updates translations array to an empty array
    await axios.put(
      `http://localhost:8000/users/${props.match.params.userId}`,
      user
    );
    setLoggedInUser({ user: user });
  }

  return (
    <div className="profileSection">
      <NavbarLogged />
      {/* The 10 latest words that were translated*/}
      <form className="boxButton" onSubmit={handleDelete}>
        <div className="wordsBox">
          <h4 className="translationTitle">Your 10 latest translations:</h4>
          <ul>
            {(loggedInUser.user.translations.length <= 10
              ? loggedInUser.user.translations
              : loggedInUser.user.translations.slice(
                  loggedInUser.user.translations.length - 10,
                  loggedInUser.user.translations.length
                )
            ).map((translation, index) => {
              return (
                <li className="wordsList" key={index}>
                  {translation}
                </li>
              );
            })}
          </ul>
        </div>
        <div className="buttonBacktoTranslate">
          <button className="btn btn-danger" type="submit">
            Delete All
          </button>
          <Link
            className="backToTranslation"
            to={"/translate/" + loggedInUser.user.id}
          >
            Back to Translation Page
          </Link>
        </div>
      </form>
    </div>
  );
}

export default Profile;
